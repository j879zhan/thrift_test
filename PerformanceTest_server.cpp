#include <atomic>
#include <fstream>
#include <csignal>

#include <thrift/protocol/TBinaryProtocol.h>
#include <thrift/server/TNonblockingServer.h>
#include <thrift/transport/TBufferTransports.h>
#include <thrift/transport/TNonblockingServerSocket.h>
#include <thrift/transport/TNonblockingServerTransport.h>
#include <thrift/concurrency/ThreadManager.h>
#include <thrift/concurrency/ThreadFactory.h>

#include "PerformanceTest.h"

using namespace ::apache::thrift;
using namespace ::apache::thrift::protocol;
using namespace ::apache::thrift::transport;
using namespace ::apache::thrift::server;

class PerformanceTestHandler : virtual public PerformanceTestIf {
    int size = 0;

public:
    static std::atomic<int64_t> execution_count;

    PerformanceTestHandler(int payload_size) { size = payload_size; }

    int64_t simple_return_int(const TEST_PACKET_SIMPLE &arg1) {
        (this->execution_count)++;
        return arg1.test_id;
    }

    void int_return_simple(TEST_PACKET_SIMPLE &_return, const int64_t id) {
        _return.test_id = id;
        _return.payload.insert(_return.payload.end(), size, 1);
        (this->execution_count)++;
        return;
    }

    void simple_return_simple(TEST_PACKET_SIMPLE &_return,
                              const TEST_PACKET_SIMPLE &arg1) {
        _return = arg1;
        (this->execution_count)++;
        return;
    }
};

std::atomic<int64_t> PerformanceTestHandler::execution_count(0);

class PerformanceTestFactory : virtual public PerformanceTestIfFactory {
    int payloadsize = 0;

public:
    ~PerformanceTestFactory() override = default;
    void setpayload(int s) { payloadsize = s; }
    PerformanceTestIf *getHandler(const TConnectionInfo &connInfo) override {
        return new PerformanceTestHandler(payloadsize);
    }
    void releaseHandler(PerformanceTestIf *handler) { delete handler; }
};

void finale() {
    std::ofstream s;
    s.open("Server_TotalRequest");
    s << PerformanceTestHandler::execution_count.load() << std::endl;
}

class SignalHandler {
public:
    static TNonblockingServer* server;

    static void Handle(int sig) {
        if (server != nullptr) {
            server->stop();
        }
    }
};

TNonblockingServer* SignalHandler::server = nullptr;

int main(int argc, char **argv) {
    int port = 9090;

    int payloadsize = 10;
    int worker = 10;
    if (argc == 2) {
        payloadsize = atoi(argv[1]);
    }
    if (argc == 3) {
        payloadsize = atoi(argv[1]);
        worker = atoi(argv[2]);
    }

    std::atexit(finale);
    std::signal(SIGTERM, SignalHandler::Handle);
    std::signal(SIGINT, SignalHandler::Handle);

    auto soc = std::make_shared<TNonblockingServerSocket>(9090);
    auto proc = std::make_shared<PerformanceTestFactory>();
    proc->setpayload(payloadsize);

    auto proc_fac = std::make_shared<PerformanceTestProcessorFactory>(proc);
    auto proto_fac = std::make_shared<TBinaryProtocolFactory>(0, 0, true, true);

    auto threadmanager = ThreadManager::newSimpleThreadManager(worker);
    threadmanager->threadFactory(std::make_shared<ThreadFactory>());
    threadmanager->start();

    TNonblockingServer server(proc_fac, proto_fac, soc, threadmanager);
    server.setNumIOThreads(1);

    SignalHandler::server = &server;

    server.serve();
    return 0;
}
