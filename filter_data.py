import pandas as pd
import numpy as np
import glob

import sys
numclient = sys.argv[1]

listdf = []

max_start = None
min_end = None

for f in glob.glob('*_latency'):
    df = pd.read_csv(f, header=None, names=['Time', 'Latency'], dtype={'Time':np.long, 'Latency':np.float64})
    ts1 = df['Time'].iloc[0]
    ts2 = df['Time'].iloc[-1]
    if max_start is None or ts1 > max_start:
        max_start = ts1
    if min_end is None or ts2 < min_end:
        min_end = ts2
    
    listdf.append(df)

for d in listdf:
    d = d.loc[(d['Time'] >= max_start) & (d['Time'] <= min_end)]

df = pd.concat(listdf)
df.sort_values(['Time'])

df.to_csv('client' + numclient + '.csv', index=False)


